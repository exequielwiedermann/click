import 'package:flutter/material.dart';

//Vamos a crear un statefulWidget
//El statefulWidget sirve para manejar los estados en una ventana
class MyButton extends StatefulWidget {
  //Este widget StatefulWidget, maneja un patrón de diseño creacional
  @override
  //Método createState le estamos devolviendo una instancia de MyButtonState,
  //después de esto lee el index y después la coleccion para leer las
  //palabras
  _MyButtoState createState() => _MyButtoState();
}

//Nosotros estamos utitilizando la clase state
//y sus funcionales en la clase MyButton
class _MyButtoState extends State<MyButton> {
  //Esto es una variable
  String flutterText = "";

  //Otra variable
  int index = 0;
  //Esto es un array en flutter
  List<String> collections = ['Flutter', 'es', 'lo más'];

  //Los valores de la segunda clase no se pierdan, lo que hace flutter es
  //renderizando
  //La variable que cambiamos es la del index
  void onPressButton() {
    //Cuando utilizamos setState, lo que hace es cambiar a State
    setState(() {
      flutterText = collections[index];

      //Esto es una condición de repetiva
      index = index < 2 ? index + 1 : 0;
      //Traza del index
      //index = 0 => Flutter
      //index = 1 => es
      //index = 2 => lo más
      //index = 0

      //? es como decir entonces
      //: es como decir un recorrer
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Este es un Stateful Widget",
        ),
        backgroundColor: Colors.orangeAccent,
      ),
      body: Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                flutterText,
                style: TextStyle(fontSize: 40.0, color: Colors.yellow),
              ),
              Padding(padding: EdgeInsets.all(10.0)),
              //Este es de la forma vieja
              //RaisedButton(onPressed: onPressed)
              //El RaisedButton se reemplazo por ElevatedButton
              ElevatedButton(
                child: Text(
                  "Actualizar",
                  style: TextStyle(fontSize: 60.0, color: Colors.brown),
                ),
                //onPressed es para realizar el comportamiento al botón
                onPressed: onPressButton,
                style: ElevatedButton.styleFrom(
                    primary: Colors.yellow,
                    padding: EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                    textStyle:
                        TextStyle(fontSize: 30, fontWeight: FontWeight.bold)),
              )
            ],
          ),
        ),
      ),
    );
  }
}
